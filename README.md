1. Aries Javascript framework does not support JSON-LD in AIP1.0, hence JSON-LD is out of scope for July 2022 release. JSON-LD will be supported in AIP 2.0 version. 

2. Also The Profile manager can not be develop due to depricated feature in [Indy lеdger](https://hyperledger.github.io/indy-did-method/#indy-ledger-objects-glossary) : ATTRIB - Deprecated

The Public Profile is an endpoint listed in the DID Document of the Participant. It returns a JSON-LD Verifiable Presentation that contains various Verifiable Credentials - those VC, the Participant marked as publicly available. 

The Self-Description served by the Public Profile can be extended through additional JSON-LD contexts. A Private / Permissioned Endpoint to serve Self-Description to authenticated requests is out of scope for iteration 1 and targeted for future releases.

    1. IDM.OCM.00030 Create Public Profile Endpoint 
    2. IDM.OCM.00031 Update Public Profile Endpoint 
    3. IDM.OCM.00032 Delete Public Profile Endpoint 
    4. IDM.OCM.00033 Update Self-Description 
    5. IDM.OCM.00034 Serve Public Profile 
    6. IDM.OCM.00035 Unserve Public Profile 
    7. IDM.OCM.00036 CRUD Service-Offering Endpoints 
    8. IDM.OCM.00037 Publish Service-Offering Endpoint 
    9. IDM.OCM.00038 Un-Publish Service-Offering Endpoint 
    10.IDM.OCM.00094 CredDefProofType 
    11.IDM.OCM.00095 Self-Description Content 
    12.IDM.OCM.00096 Self-Description Output 
    13.IDM.OCM.00097 Service-Offering CRUD 
    14.IDM.OCM.00098 Service Offering Export
    
## Dependencies
<hr/>

[Dependencies](package.json)

## License
<hr/>

[Apache 2.0 license](LICENSE)
