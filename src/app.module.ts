import { APP_FILTER } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { config } from '../config/config';
import { validationSchema } from '../config/validation';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health/health.controller';
import { LiquibaseController } from './liquibase/liquibase.controller';
import { ExceptionHandler } from './common/exception.handler';
import { ProfilesModule } from './profile/module';
// import { PostsModule } from './posts/posts.module';

@Module({
  imports: [
    TerminusModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
      envFilePath: `${process.cwd()}/config/env/${process.env.NODE_ENV}.env`,
      validationSchema: validationSchema,
    }),
    ProfilesModule,
  ],
  controllers: [HealthController, LiquibaseController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: ExceptionHandler,
    },
  ],
})
export class AppModule {}
