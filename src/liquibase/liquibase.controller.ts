import { Controller, Get, Version } from '@nestjs/common';
import {
  LiquibaseConfig,
  Liquibase,
  POSTGRESQL_DEFAULT_CONFIG,
  LiquibaseLogLevels,
} from 'liquibase';

const liquibaseConfig: LiquibaseConfig = {
  ...POSTGRESQL_DEFAULT_CONFIG,
  changeLogFile: './changelog.xml',
  url: 'jdbc:postgresql://localhost:5432/dbname',
  username: 'postgres',
  password: 'password',
  logLevel: LiquibaseLogLevels.Info,
};
const liquibaseInstance = new Liquibase(liquibaseConfig);

@Controller('liquibase')
export class LiquibaseController {
  @Version(['1', '2'])
  @Get('getStatus')
  async getStatus() {
    console.log('Check Liquibase called');
    return await liquibaseInstance.status();
  }

  @Version(['1', '2'])
  @Get('updateScript')
  async updateScript() {
    console.log('Update Script called');
    return liquibaseInstance.update({ labels: '158' });
  }

  @Version(['1', '2'])
  @Get('dropAll')
  async dropAll() {
    console.log('Drop All called');
    return liquibaseInstance.dropAll();
  }

  @Version(['1', '2'])
  @Get('getHistory')
  async getHistory() {
    console.log(liquibaseInstance.history());
    return liquibaseInstance.history();
  }
}
