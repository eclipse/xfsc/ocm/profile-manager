import { Module } from '@nestjs/common';
import { ProfilesService } from './services/service';
import { ProfilesController } from './controller/controller';
import { PrismaService } from 'src/prisma/prisma.service';

@Module({
  controllers: [ProfilesController],
  providers: [ProfilesService, PrismaService],
})
export class ProfilesModule {}
