import { Injectable } from '@nestjs/common';
import { ProfileDto } from '../entities/entity';
import { ProfileRepository } from '../repository/profile.respository';
import { PrismaService } from '../../prisma/prisma.service';

@Injectable()
export class ProfilesService {
  private profileRepository;
  constructor(private readonly prismaService: PrismaService) {
    this.profileRepository = new ProfileRepository(this.prismaService);
  }

  async createProfiles(profile: ProfileDto) {
    return await this.profileRepository.createProfile(profile);
  }

  async findProfiles() {
    return await this.profileRepository.findProfiles();
  }
}
